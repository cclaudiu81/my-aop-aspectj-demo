package com.cc.myaop.learning;

/**
 * How many times is the method declared in the SimpleDemo Interface invoked?
 * 
 * Lets count the occurences of method from the {@link SimpleDemo} interface which are
 * invoked using AOP ;)
 * 
 * Keep in mind this Aspect is a Singleton by default
 * 
 * @author cclaudiu
 */
public aspect CountMethodCalls {

	int countTraceComputeNumber;
	int countTraceComputeString;
	
	pointcut countComputeNumber() : 
		call(void SimpleDemo.computeNumber());
	
	before() : countComputeNumber() {
		countTraceComputeNumber++;
	}
	
	before() : 
		call(void SimpleDemo.computeString()) {
		countTraceComputeString++;
	}
	
	after() :
		call(void SimpleDemo.computeString()) {
		System.out.println("Method countComputeNumber");
	}
}