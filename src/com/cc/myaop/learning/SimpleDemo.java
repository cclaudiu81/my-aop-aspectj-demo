package com.cc.myaop.learning;

public interface SimpleDemo {

	void computeString(String name);

	void computeNumber(int number);

}
