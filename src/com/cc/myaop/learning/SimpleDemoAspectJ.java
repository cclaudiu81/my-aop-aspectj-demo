package com.cc.myaop.learning;

public class SimpleDemoAspectJ implements SimpleDemo {

	@Override
	public void computeNumber(int number) {
		System.out.println("Printing number: " + number);
	}
	
	@Override
	public void computeString(String name) {
		System.out.println("Printing name: " + name);
	}
	
	public static void main(String[] args) {
		SimpleDemo client = new SimpleDemoAspectJ();
		
		client.computeNumber(24);
		client.computeString("cclaudiu");
	}
}