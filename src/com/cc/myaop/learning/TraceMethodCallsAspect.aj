package com.cc.myaop.learning;

/**
 * To actually implement crosscuting behavior we'll implement Advices; The
 * pointcut responsibility is to simply pick out join-points, which are points
 * in execution of a program
 * 
 * The advice is th actual code that gets executed whenever a joinpoint is met
 * AspectJ has several advices: before, after...
 * 
 * pointcuts pick out join points. But they don't do anything apart from picking
 * out join points.
 * 
 * To actually implement crosscuting concerns we use << Advices >> Advices in
 * AspectJ are of several Types: 
 * --> before() : runs before a method is executed, after the args of a method are evaluated 
 * --> after() : runs after a particular joinpoint. 
 * --> around()
 * 
 * Aspects wrap up pointcuts, advice, and inter-type declarations in a a modular
 * unit of crosscutting implementation. It is defined very much like a class,
 * and can have methods, fields, and initializers in addition to the
 * crosscutting members.
 * 
 * By default, each aspect is a singleton
 * 
 * Using AspectJ makes it possible to cleanly modularize this kind of
 * functionality(logging, tracing, debugging)
 * 
 * Special attention: The individual methods that are traced do not need to be changed, when tracing operations
 * need to be changed.
 * 
 * @see SimpleDemo is an interface in this context
 * 
 * @author cclaudiu
 * 
 */
public aspect TraceMethodCallsAspect {

	pointcut traceComputeNumber() : 
		call(void SimpleDemo.computeNumber(int));

	pointcut traceComputeString() :
		call(void SimpleDemo.computeString(String));

	pointcut traceComputeNumberExplicitly() : 
		call(void SimpleDemo.computeNumber(int));

	/**
	 * trace all methods from the interface
	 */
	pointcut traceAll() :
		call(void SimpleDemo.*(..));

	pointcut traceMethodsStartingWithTraceCompute() :
		call(void SimpleDemo.trace*(..));

	before() : traceComputeNumber() {
		System.out.println("Tracing pointcut traceSimpleCompute");
	}

	after() : traceComputeString() {
		System.out.println("Executed after joinpoint is met");
	}

	before() : traceAll() {
		System.out
				.println("Tracing all methods of the Interface SimpleDemo that starts with [traceCompute]");
	}

	after() returning: traceComputeString() {
		System.out
				.println("Advice executed after the method traceComputeString returns");
	}

	before() : traceComputeNumber() {
		System.out.println("Executing: " + thisJoinPoint);
	}
}