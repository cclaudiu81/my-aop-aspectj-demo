package com.cc.myaop.learning.designbycontract;

public aspect ContractAssertAspect {
	
	pointcut preconditionDbC(String str) : 
		call(void SomeService.computeWithAop(String)) && args(str);
	
	before(String str) : preconditionDbC(str) {
		if(str == null) {
			throw new IllegalArgumentException();
		}
	}
}