package com.cc.myaop.learning.designbycontract;

/**
 * To enable a more stricly contract we can enforce a method call on a new instance, and enforce the contract
 * that the client should call compute() only on a instance produced by the factory method.
 * 
 * Rather than using the "call" aop notation, we'll use the "withincode" notation for specifying the pattern
 * 
 * @author cclaudiu
 */
public aspect ContractEnforcementAspect {
	
	/* declare the first pointcut to mark the joinpoint when AOP should fire/handle the event */
	pointcut callComputeOnNewInstance() : 
		call(void FooService.compute(String));

	/* declare the pointcut for the static factory method idiom this should always be invoked before calling compute() */
	pointcut accessThroughFactoryInstance() : 
		withincode(static FooService FooService.createInstance());
	
	/* signal an error for any calls to compute() that are not within the factory method createInstance. */
//	before(): callComputeOnNewInstance() && !accessThroughFactoryInstance() {
//		throw new IllegalArgumentException("Cannot call compute() on a new instance of FooService!" + thisJoinPoint);
//	}
	
	/** 
	 * another advice which is more consistently that AOP features constraints the client from not compiling
	 * the code if one of the test-condition is met
	 */
	declare error: callComputeOnNewInstance() && !accessThroughFactoryInstance():
		"Illegal invocation of compute() outside the factory method";
	
}