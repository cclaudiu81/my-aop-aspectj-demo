package com.cc.myaop.learning.designbycontract;

/**
 * The compute() method should ONLY be accessed whithin the createInstance() factory method, and not directly
 * on ANY instance of FooService!
 * 
 * @author cclaudiu
 */
public class FooService {
	
	public void compute(String s) {
		System.out.println("Accessing the compute(String) method only through the static factory! " + s);
	}
	
	public static final FooService createInstance() {
		FooService instance = new FooService();
		instance.compute("cclaudiu");
		return instance;
	}
}