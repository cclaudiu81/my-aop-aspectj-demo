package com.cc.myaop.learning.designbycontract;

public class FooServiceTest {
	
	public static void main(String[] args) {
		FooService anotherInstance = FooService.createInstance();
		
		// uncomment this line for a better protection ;)
//		anotherInstance.compute("this joinpoint throws an exception!");
	}
}