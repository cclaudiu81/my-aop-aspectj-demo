package com.cc.myaop.learning.designbycontract;

/**
 * First Method does not use the AOP(crosscutting-concerns): therefore pollutes the concerns into one place, and has two responsibilities
 * that is one for validating the method-arguments, and one responsibility for doing the actual work logic
 * 
 * While the second method simply implements the actual work/processing the parameters. Second scenario
 * decouples, and aligns itself to the ORTOGONALITY Principle, separating the concerns.
 * 
 * @author cclaudiu
 *
 */
public class SomeService {
	
	void computeWithoutAop(String s) {
		if(s == null) {
			throw new IllegalArgumentException();
		}
		
		System.out.println(s.toLowerCase());
	}
	
	void computeWithAop(String s) {
		System.out.println(s.toLowerCase());
	}
	
	// Test
	public static void main(String[] args) {
		SomeService service = new SomeService();
		
//		service.computeWithoutAop(null);
		service.computeWithAop(null);
	}
}