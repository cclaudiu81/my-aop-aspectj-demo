package com.cc.myaop.learning.exceptionhandler;

import java.io.OutputStream;

/**
 * Aspect can be abstract, they are the unit-of-modularization for AOP, hence they can behave as java classes
 * they can be extended, declare behavior, state for the aspect; the only difference is that they cannot be directly
 * instantiated by the clients. AOP internal engine handles this, and by default a single instance is created for-each
 * aspect.
 * 
 * @author cclaudiu
 *
 */
public abstract aspect AbstractComputationAspect {

	public abstract pointcut printStreamPointcut(OutputStream os);
	
	before(OutputStream os) : printStreamPointcut(os) {
		System.out.println("Before: printing Hello dude! ..... and then printing: " + os);
	}
}