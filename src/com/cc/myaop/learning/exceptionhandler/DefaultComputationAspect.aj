package com.cc.myaop.learning.exceptionhandler;

import java.io.OutputStream;

/**
 * Deferred behavior implementation of the pointcut to the DefaultComputationAspect
 * Hence the default implementation will establish the "rules" of when the pointcut's joinpoints are
 * met and the advice is executed;
 * Think of the pointcuts as an events, which are handled by the "attached" advices.
 * 
 * @author cclaudiu
 */
public aspect DefaultComputationAspect extends AbstractComputationAspect {
	
	public pointcut printStreamPointcut(OutputStream os):
		withincode(void ExceptionContext.compute(String)) && args(os);
}