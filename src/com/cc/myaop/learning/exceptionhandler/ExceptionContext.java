package com.cc.myaop.learning.exceptionhandler;

public class ExceptionContext {
	
	void compute(String someString) {
		if(someString.endsWith("cosar")) {
			throw new IllegalStateException("Name cannot end in [cosar]");
		}
		System.out.println("Processing...." + someString);
	}
	
	public static void main(String[] args) {
//		new ExceptionContext().compute("cosar");
		new ExceptionContext().compute("claudiu");
	}
}