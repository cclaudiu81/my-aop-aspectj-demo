package com.cc.myaop.learning.exceptionhandler;

// TODO: handle the AOP exceptionHandler!

public aspect ExceptionHandlerAspect {
	
	pointcut computePointcut() : 
		execution(void ExceptionContext.compute(..)) && within(ExceptionContext);
	
	after() throwing(IllegalStateException e): computePointcut() {
		System.out.println("AFTER=Handling exception from within AOP: " + e.getMessage());
	}
	
	pointcut myExceptionHandler(IllegalStateException e) :
		execution(* Exception*.*(..)) && handler(IllegalStateException) && args(e);
	
	after(IllegalStateException e): myExceptionHandler(e) {
		System.out.println("AFTER=Handling exception from within AOP: " + e.getMessage());
	}
}
