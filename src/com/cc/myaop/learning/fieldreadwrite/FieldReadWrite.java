package com.cc.myaop.learning.fieldreadwrite;

public class FieldReadWrite {
	private String name;
	
	public FieldReadWrite(String name) {
		setName(name);
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public String getName() {
		return name;
	}
	
	public static void main(String[] args) {
		FieldReadWrite instance = new FieldReadWrite("cclaudiu");
		instance.getName();
	}
}

//Here's the output. Note that all output is from within the Aspect!
//Trying to assign the variable name within constructor: cclaudiu
//Trying to set variable [name] to: cclaudiu
//Variable name has been asssigned to: cclaudiu
//Accessed variable name: cclaudiu
