package com.cc.myaop.learning.fieldreadwrite;

public aspect FieldReadWriteAspect {

	// Method Accessor/Mutator pointcut -- aggregates the calls/points in the program execution
	pointcut setNamePointcut(String newName) : 
		execution(public void FieldReadWrite.setName(..)) && args(newName);
	
	pointcut getNamePointcut() :
		execution(public * FieldReadWrite.getName(..));
	
	// Constructor Pointcut -- aggregate the joinpoint-events and setup the Context(in this case a String)
	pointcut assignNameFromConstructor(String newName) :
		execution(public FieldReadWrite.new(String)) && args(newName);
	
	after(String name) returning() : setNamePointcut(name) {
		System.out.println("Variable name has been asssigned to: " + name);
	}
	
	before(String newName) : setNamePointcut(newName) {
		System.out.println("Trying to set variable [name] to: " + newName);
	}
	
	after() returning(String name) : getNamePointcut() {
		System.out.println("Accessed variable name: " + name);
	}
	
	// Constructor Advice for setting the new Variable -- note that the constructor uses the setter
	before(String context) : assignNameFromConstructor(context) {
		System.out.println("Trying to assign the variable name within constructor: " + context);
	}
}