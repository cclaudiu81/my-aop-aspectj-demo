package com.cc.myaop.learning.logging;

public interface AuthService {

	void login(MySubject subject);
	
	void logout(MySubject subject);
}
