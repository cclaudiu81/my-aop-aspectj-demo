package com.cc.myaop.learning.logging;

/**
 * Utility Design by Contract Class
 * 
 * @author cclaudiu
 */
public class ContractAssert {

	private ContractAssert() { }
	
	public static final void notNull(Object o, String message) {
		if(o == null) {
			throw new IllegalArgumentException(message);
		}
	}
}