package com.cc.myaop.learning.logging;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Using the java.util.logging -- do not use this in real env! prefer slf4j
 * @author cclaudiu
 *
 */
public class DefaultAuthService implements AuthService {

	private static final Logger LOG = Logger.getLogger(DefaultAuthService.class.getName());
	private Map<String, MySubject> cached = new HashMap<>();
	
	@Override
	public void login(MySubject subject) {
		ContractAssert.notNull(subject, "Subject cannot be null!");
		LOG.info("Entering login() method, Trying to authenticate user: " + subject.getUsername());
		doLogin(subject);
		LOG.info("Successfully logged in user: " + subject.getUsername());
	}

	@Override
	public void logout(MySubject subject) {
		ContractAssert.notNull(subject, "User cannot be null! in attempt for logging out");
		LOG.info("Trying to logout user: " + subject.getUsername());
		
		// actual core operation -- that is the subsystem core concern implementation
		if(cached.containsKey(subject.getUsername())) {
			System.out.println("Logged out! the user session should be destroyed now!");
		}
		
		LOG.info("Successfully logged out user: " + subject.getUsername());
		
	}

	private boolean doLogin(MySubject subject) {
		// a database operation
		if(subject.getUsername().equalsIgnoreCase("cclaudiu")) {
			cached.put(subject.getUsername(), subject);
			return true;
		}
		return false;
	}
}
