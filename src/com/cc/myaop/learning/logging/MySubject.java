package com.cc.myaop.learning.logging;

/**
 * Immutable Subject -- provides the Value Object of the application-concern
 * 
 * @author cclaudiu
 */
public class MySubject {

	private final char[] passwd;
	private final String username;

	public MySubject(String username, char[] passwd) {
		this.username = username;
		this.passwd = passwd;
	}

	public char[] getPasswd() {
		return passwd;
	}

	public String getUsername() {
		return username;
	}
}