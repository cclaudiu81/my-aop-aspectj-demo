package com.cc.myaop.learning.logging.withaop;

import java.util.HashMap;
import java.util.Map;
import com.cc.myaop.learning.logging.AuthService;
import com.cc.myaop.learning.logging.MySubject;

public class AopAuthServiceImpl implements AuthService {
	private Map<String, MySubject> cached = new HashMap<>();

	@Override
	public void login(MySubject subject) {
		doLogin(subject);
	}

	@Override
	public void logout(MySubject subject) {
		doLogout(subject);
	}

	// --------- delegated method for doing the actual login/logout --------------//
	private void doLogout(MySubject subject) {
		if(cached.containsKey(subject.getUsername())) {
			System.out.println("Logged out! the user session should be destroyed now!");
			cached.remove(subject.getUsername());
		}
	}

	private boolean doLogin(MySubject subject) {
		if(subject.getUsername().equalsIgnoreCase("cclaudiu")) {
			cached.put(subject.getUsername(), subject);
			return true;
		}
		return false;
	}
}