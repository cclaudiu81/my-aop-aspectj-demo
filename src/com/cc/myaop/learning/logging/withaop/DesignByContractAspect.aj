package com.cc.myaop.learning.logging.withaop;

import com.cc.myaop.learning.logging.MySubject;

public aspect DesignByContractAspect {
	
	pointcut notNullPointcut(MySubject subject) : 
		execution(public void  com.cc.myaop.learning.logging.AuthService.log*(..)) && args(subject);
	
	before(MySubject subject): notNullPointcut(subject) {
		if(subject == null) {
			throw new IllegalArgumentException("Subject cannot be null!");
		}
	}
}