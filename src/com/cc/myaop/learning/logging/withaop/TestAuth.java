package com.cc.myaop.learning.logging.withaop;

import com.cc.myaop.learning.logging.AuthService;
import com.cc.myaop.learning.logging.MySubject;

public class TestAuth {
	
	public static void main(String[] args) {
		AuthService service = new AopAuthServiceImpl();
		service.login(new MySubject("cclaudiu", new char[]{'1'}));
		
		service.logout(new MySubject("cclaudiu", new char[]{'1'}));
	}

}

// and the output clears the AOP modularization:
// LOGIN:
// INFO: Entering login() method, Trying to authenticate user: cclaudiu
// INFO: Successfully logged in user: cclaudiu

// LOGOUT:
// INFO: Trying to logout user: cclaudiu
// INFO: Successfully logged out user: cclaudiu