package com.cc.myaop.learning.logging.withaop;

import java.util.logging.Logger;
import com.cc.myaop.learning.logging.MySubject;

/**
 * Take a look at the Interfaces with which the Aspect is working ;)
 * 
 * @author cclaudiu
 */
public aspect TraceAuthOperationsAspect {

	private final static Logger LOG = Logger.getLogger(TraceAuthOperationsAspect.class.getName());
	
	pointcut loginOperations(MySubject subject) : 
		call(void  com.cc.myaop.learning.logging.AuthService.login(MySubject))
		&& args(subject);
	
	pointcut logoutOperations(MySubject subject) :
		call(void  com.cc.myaop.learning.logging.AuthService.logout(MySubject))
		&& args(subject);

	
	before(MySubject subject) : loginOperations(subject){
		LOG.info("Entering login() method, Trying to authenticate user: " + subject.getUsername()) ;
	}
	
	after(MySubject subject) returning : loginOperations(subject) {
		LOG.info("Successfully logged in user: " + subject.getUsername());
	}
	
	
	before(MySubject subject) : logoutOperations(subject){
		LOG.info("Trying to logout user: " + subject.getUsername());
	}
	
	after(MySubject subject) returning : logoutOperations(subject) {
		LOG.info("Successfully logged out user: " + subject.getUsername());
	}
}



